<?php

use Illuminate\Support\Facades\Route;


Route::get('/call-logs/{status}/{date}', 'TestController@callLog');
Route::get('/date-vs-no-of-call/{status}/{date}', 'TestController@dateVsNoOfCall');


Route::get('/{any}', function () {
  return view('post');
})->where('any', '.*');

