
# Tasks
- 1. List of 500 phone call logs in a grid where fields are call date, phone
 number, call duration & status.
- 2. The same page gives a timeline chart
- a. Date vs count Number of calls
- b. Date Range filter
- 3. Filter based on status & date range on the same page.
- 4. Grid & Timeline chart will be affected based on the dropdown status and
date range selected.
- 5. For data, just create a static JSON format with 500 records or Axios calls  using a database (preferable).


# Using
- Laravel 7.12
- Mysql
- Vuejs 2 
- axios


# to run

- npm i
- npm run dev
- php artisan serve

