require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import HomeComponent from './components/HomeComponent.vue';


const routes = [
  {      
      path: '/home',
	  name: 'home',
      component: HomeComponent
  }
];

const router = new VueRouter({ 
						mode: 'history', 
						routes: routes
					});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');


