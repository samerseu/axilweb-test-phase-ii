<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallLog extends Model
{
    protected $fillable = [
		'id',
		'call_date',
		'phone_number',
		'call_duration',
		'status'
	];
	protected $table = "call_logs";
}
