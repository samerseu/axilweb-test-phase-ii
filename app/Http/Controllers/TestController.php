<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CallLog;

class TestController extends Controller
{		
    public function vueTest()  {
		return view('sample-vue-with-cdn');
	}
	
		
	
	
	
	/*************************
		api for call logs
	**************************/
	public function callLog($status, $date) {		
		//echo "$status,$date";
		$query= CallLog::select([
			'id',
			'call_date', 
			'phone_number' , 
			'call_duration', 
			'status'			
		]);
		if ($status == 'all') {
			if ($date == '*') {
				  $callLogs = $query
				->orderBy('id', 'desc')
				->get();
			}else {
				  $callLogs = $query
				  ->where('call_date', $date)
				->orderBy('id', 'desc')
				->get();
			}
		} else {
			if ($date == '*') {
				$callLogs = $query
		  	    	->where('status', $status)
				->orderBy('id', 'desc')
				->get();
			}else {
				$callLogs = $query
					->where('call_date', $date)
		  	    	->where('status', $status)
				->orderBy('id', 'desc')
				->get();
				//echo "block343";
			}	
	    }
	    //dd($callLogs);
		//exit();		
		return response()
				->json($callLogs);
	}
	
	/******************************
		api for date vs no of call
	********************************/
	public function dateVsNoOfCall($status, $date) {	
	//echo 'from-server2:'. $status;	
		$query = CallLog::select([
			'call_date', 
			\DB::raw('count(*) as no_of_call')			
		]);
		if ($status == 'all') { 
			if ($date == '*') {
			$callLogs = $query		
				->groupBy('call_date')				
				->get();	
			} else {
						$callLogs = $query		
						->where('call_date', $date)
				->groupBy('call_date')				
				->get();		
			}	
		}else{
			if ($date == '*') {
			  $callLogs = $query
				->where('status', $status)
				->groupBy('call_date')		
				->get();
			}else {
               $callLogs = $query
               ->where('call_date', $date)
				->where('status', $status)
				->groupBy('call_date')		
				->get();	
			}
	    }
		$labels = [];
		$data= [];
		foreach($callLogs as $c) {
			array_push($labels, $c->call_date);
			array_push($data , $c->no_of_call);
		}

		$all = [];
		$all['labels'] = $labels;
		$all['datasets'] = $data; 

		
		return response()
				->json(['all'=>$all]);
	}
	
}
